package com.tinemislej.service.db;

import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "endpoints")
@Entity
public class DbEndpoint {

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<DbConcept> inputs;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<DbConcept> outputs;

    @OneToOne(cascade = CascadeType.ALL)
    private DbNonFunctionalParameters nonFunctionalParameters;

    @Id
    @NotNull
    private String url;

    public DbEndpoint() {

    }

    public DbEndpoint(Set<DbConcept> inputs, Set<DbConcept> outputs,
            @NotNull String url, DbNonFunctionalParameters nonFunctionalParameters) {
        this.inputs = inputs;
        this.outputs = outputs;
        this.url = url;
        this.nonFunctionalParameters = nonFunctionalParameters;

    }

    public Set<DbConcept> getInputs() {
        return inputs;
    }

    public Set<DbConcept> getOutputs() {
        return outputs;
    }

    public String getUrl() {
        return url;
    }

    public DbNonFunctionalParameters getNonFunctionalParameters() {
        return nonFunctionalParameters;
    }
}
