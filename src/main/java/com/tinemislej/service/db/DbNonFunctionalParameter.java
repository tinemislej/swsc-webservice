package com.tinemislej.service.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class DbNonFunctionalParameter {

    @Id @GeneratedValue(strategy = GenerationType.AUTO) Long id;

    public DbNonFunctionalParameter() {
    }
}
