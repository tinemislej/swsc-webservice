package com.tinemislej.service.db;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "subconcepts")
public class DbSubconcept {

    @Id
    @NotNull
    private String name;

    public DbSubconcept(@NotNull String name) {
        this.name = name;
    }

    public DbSubconcept(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
