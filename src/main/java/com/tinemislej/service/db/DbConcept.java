package com.tinemislej.service.db;


import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "concepts")
public class DbConcept {

    @Id
    @NotNull
    private String concept;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<DbSubconcept> subconcepts;

    public DbConcept(String concept, Set<DbSubconcept> subconcepts){
        this.concept = concept;
        this.subconcepts = subconcepts;
    }

    public DbConcept(){}

    public String getConcept() {
        return concept;
    }

    public Set<DbSubconcept> getSubconcepts() {
        return subconcepts;
    }
}
