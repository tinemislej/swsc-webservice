package com.tinemislej.service.db.repository;

import com.tinemislej.service.db.DbConcept;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConceptRepository extends JpaRepository<DbConcept, String>{}
