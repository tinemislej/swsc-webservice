package com.tinemislej.service.db;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "nonfunctionalparams")
public class DbNonFunctionalParameters {

    @Id @GeneratedValue(strategy= GenerationType.AUTO) Long id;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = DbNonFunctionalParameter.class)
    private List<DbNonFunctionalParameter> parameters;

    public DbNonFunctionalParameters(List<DbNonFunctionalParameter> parameters) {
        this.parameters = parameters;
    }

    public DbNonFunctionalParameters() {
    }

    public List<DbNonFunctionalParameter> getParameters() {
        return parameters;
    }
}
