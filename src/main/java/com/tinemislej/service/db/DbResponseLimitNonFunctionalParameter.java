package com.tinemislej.service.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Table(name = "responselimitnonfunctionalparameters")
@Entity
public class DbResponseLimitNonFunctionalParameter extends DbNonFunctionalParameter {

    private String type;

    @Column(name = "val")
    private int limit;


    @Column(name = "ldkey")
    private String key;

    public DbResponseLimitNonFunctionalParameter(String key, String type, int limit) {
        this.type = type;
        this.limit = limit;
        this.key = key;
    }

    public DbResponseLimitNonFunctionalParameter() {
    }

    public String getType() {
        return type;
    }

    public int getLimit() {
        return limit;
    }

    public String getKey() {
        return key;
    }
}
