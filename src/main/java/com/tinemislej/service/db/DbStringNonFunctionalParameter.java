package com.tinemislej.service.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Table(name = "stringnonfunctionalparameters")
@Entity
public class DbStringNonFunctionalParameter extends DbNonFunctionalParameter {

    private String value;

    @Column(name = "ldkey")
    private String key;

    public DbStringNonFunctionalParameter(String key, String value) {
        this.value = value;
        this.key = key;
    }

    public DbStringNonFunctionalParameter() {
    }

    public String getValue() {
        return value;
    }

    public String getKey() {
        return key;
    }
}
