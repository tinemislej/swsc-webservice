package com.tinemislej.service.controllers;

import com.github.jsonldjava.core.JsonLdError;
import com.tinemislej.service.components.match.Matcher;
import com.tinemislej.service.components.parse.api.ApiParserImpl;
import com.tinemislej.service.components.parse.api.model.Endpoint;
import com.tinemislej.service.components.parse.api.model.NonFunctionalParameter;
import com.tinemislej.service.components.parse.api.model.NonFunctionalParameters;
import com.tinemislej.service.components.parse.api.model.ResponseLimitNonFunctionalParameter;
import com.tinemislej.service.components.parse.api.model.StringNonFunctionalParameter;
import com.tinemislej.service.components.parse.endpoint.EndpointParser;
import com.tinemislej.service.components.parse.endpoint.model.Concept;
import com.tinemislej.service.components.parse.endpoint.model.UiEndpoint;
import com.tinemislej.service.components.parse.reference.ReferenceProcessor;
import com.tinemislej.service.db.DbConcept;
import com.tinemislej.service.db.DbEndpoint;
import com.tinemislej.service.db.DbNonFunctionalParameter;
import com.tinemislej.service.db.DbNonFunctionalParameters;
import com.tinemislej.service.db.DbResponseLimitNonFunctionalParameter;
import com.tinemislej.service.db.DbStringNonFunctionalParameter;
import com.tinemislej.service.db.DbSubconcept;
import com.tinemislej.service.db.repository.EndpointRepository;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import me.andrz.jackson.JsonReferenceException;


@CrossOrigin
@RestController
public class FileController {

    ApiParserImpl parser = new ApiParserImpl();
    EndpointParser endpointParser = new EndpointParser(new Matcher());
    ReferenceProcessor processor = new ReferenceProcessor();

    @Autowired EndpointRepository repository;


    @PostMapping(value = "/addSwagger") public String fileUpload(@RequestParam("file") MultipartFile file,
            RedirectAttributes redirectAttributes) throws IOException, JsonLdError, JsonReferenceException {
        File f = new File("temp.json");
        f.createNewFile();
        FileUtils.writeByteArrayToFile(f, file.getBytes());
        String dereferencedSource = processor.process(f);
        f.delete();

        List<Endpoint> endpoints = parser.parseApiDefinition(dereferencedSource);
        Set<UiEndpoint> e = endpointParser.parseEndpoints(endpoints);

        List<DbEndpoint> endpointList = new ArrayList<>();
        for (UiEndpoint uiEndpoint : e) {
            Set<DbConcept> inputs = parseParameter(uiEndpoint.getInputConcepts());
            Set<DbConcept> outputs = parseParameter(uiEndpoint.getOutputConcepts());

            DbNonFunctionalParameters nonFunctionalParameters = parseNonFunctionalParameters(
                    uiEndpoint.getNonFunctionalParameters());
            endpointList.add(new DbEndpoint(inputs, outputs, uiEndpoint.getUrl(), nonFunctionalParameters));
        }

        repository.saveAll(endpointList);
        return "ok";
    }

    private Set<DbConcept> parseParameter(Set<Concept> endpoint) {
        Set<DbConcept> concepts = new HashSet<>();
        for (Concept c : endpoint) {

            Set<DbSubconcept> dbSubconcepts = new HashSet<>();
            for (String subConcept : c.getConcepts()) {
                dbSubconcepts.add(new DbSubconcept(subConcept));
            }

            concepts.add(new DbConcept(c.getMainConcept(), dbSubconcepts));
        }

        return concepts;
    }

    private DbNonFunctionalParameters parseNonFunctionalParameters(
            @Nullable NonFunctionalParameters nonFunctionalParameters) {
        List<DbNonFunctionalParameter> dbNonFunctionalParameters = new ArrayList<>();
        if (nonFunctionalParameters == null) {
            return null;
        }

        for (NonFunctionalParameter p : nonFunctionalParameters.getParameters()) {
            if (p instanceof StringNonFunctionalParameter) {
                StringNonFunctionalParameter snp = (StringNonFunctionalParameter) p;
                dbNonFunctionalParameters.add(
                        new DbStringNonFunctionalParameter(p.getUri(), ((StringNonFunctionalParameter) p).getValue()));
            } else {
                ResponseLimitNonFunctionalParameter rlnp = (ResponseLimitNonFunctionalParameter) p;
                dbNonFunctionalParameters.add(
                        new DbResponseLimitNonFunctionalParameter(rlnp.getUri(), rlnp.getValue().getType(),
                                rlnp.getValue().getValue()));
            }
        }

        return new DbNonFunctionalParameters(dbNonFunctionalParameters);
    }
}
