package com.tinemislej.service.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tinemislej.service.components.CandidateScorer;
import com.tinemislej.service.components.compose.LayerComposer;
import com.tinemislej.service.components.compose.PossibleSolutionFinder;
import com.tinemislej.service.components.compose.PossibleSolutionFinder.CompositionSolution;
import com.tinemislej.service.components.compose.Pruner;
import com.tinemislej.service.components.compose.Splitter;
import com.tinemislej.service.components.compose.model.DummyNode;
import com.tinemislej.service.components.compose.model.EndpointNode;
import com.tinemislej.service.components.compose.model.GraphResult;
import com.tinemislej.service.components.compose.model.Node;
import com.tinemislej.service.components.match.Matcher;
import com.tinemislej.service.components.parse.endpoint.model.Concept;
import com.tinemislej.service.components.parse.endpoint.model.UiEndpoint;
import com.tinemislej.service.components.repo.EndpointRepo;
import com.tinemislej.service.data.UnSuccessfulCompositionResponse;
import com.tinemislej.service.data.response.CandidateResponse;
import com.tinemislej.service.data.response.EdgeResponse;
import com.tinemislej.service.data.response.NodeResponse;
import com.tinemislej.service.data.response.SuccessfulCompositionResponse;
import com.tinemislej.service.db.DbConcept;
import com.tinemislej.service.db.DbEndpoint;
import com.tinemislej.service.db.DbSubconcept;

import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
public class GraphController {

    Pruner pruner = new Pruner();
    @Autowired Splitter splitter;

    @Autowired Matcher matcher;
    @Autowired CandidateScorer scorer;
    Gson gson = new GsonBuilder().create();
    @Autowired EndpointRepo endpointRepository; //= new InMemoryEndpointRepo(repo);
    @Autowired
    PossibleSolutionFinder possibleSolutionFinder; // = new PossibleSolutionFinder(endpointRepository, matcher);

    @PostMapping(value = "/getGraph", consumes = "application/json")
    public ResponseEntity<String> getGraph(@RequestBody Map<String, Object> body) {

        List<String> inputs = (List<String>) body.get("inputs");
        List<String> outputs = (List<String>) body.get("outputs");
        LayerComposer composer = new LayerComposer(new HashSet<>(endpointRepository.getEndpoints()), matcher);

        GraphResult g = composer.getGraph(inputs, new HashSet<>(outputs));
        if (g.isSuccessful()) {
            DirectedAcyclicGraph<Node, DefaultWeightedEdge> graph = composer.addDummyNodes(g.getGraph(),
                    new HashSet<>(inputs), new HashSet(outputs));
            graph = pruner.pruneGraph(graph);
            List<DirectedAcyclicGraph<Node, DefaultWeightedEdge>> results = splitter.split(graph, outputs);

            results = results.stream().filter(g1 -> {
                CycleDetector cycleDetector = new CycleDetector(g1);
                return !cycleDetector.detectCycles();
            }).collect(Collectors.toList());
            List<CandidateResponse> candidates = parseCandidate(results);
            candidates.sort((o1, o2) -> -1 * Double.compare(o1.getScore(), o2.getScore()));
            return ResponseEntity.ok(gson.toJson(new SuccessfulCompositionResponse(candidates)));
        } else {
            List<List<CompositionSolution>> sols = possibleSolutionFinder.findPossibleSolutions(g.getLeft(),
                    g.getRight(), new HashSet<>(outputs));

            //return gson.toJson(new UnSuccessfulCompositionResponse(sols));
            return new ResponseEntity<>(gson.toJson(new UnSuccessfulCompositionResponse(sols)), HttpStatus.NOT_FOUND);
        }
    }

    private List<UiEndpoint> parseEndpoints(List<DbEndpoint> dbEndpoints) {
        List<UiEndpoint> endpoints = new ArrayList<>(dbEndpoints.size());
        for (DbEndpoint dbEndpoint : dbEndpoints) {
            UiEndpoint e = new UiEndpoint(dbEndpoint.getUrl(), parse(dbEndpoint.getInputs()),
                    parse(dbEndpoint.getOutputs()));
            endpoints.add(e);
        }
        return endpoints;
    }

    private Set<Concept> parse(Set<DbConcept> concepts) {
        Set<Concept> uiConcepts = new HashSet<>();
        for (DbConcept c : concepts) {
            Set<String> subconcepts = new HashSet<>();
            for (DbSubconcept dbs : c.getSubconcepts()) {
                subconcepts.add(dbs.getName());
            }
            uiConcepts.add(new Concept(c.getConcept(), subconcepts));
        }
        return uiConcepts;
    }

    private List<CandidateResponse> parseCandidate(
            List<DirectedAcyclicGraph<Node, DefaultWeightedEdge>> graphs) {
        HashMap<String, Long> nodeIdMap = new HashMap<>();
        List<CandidateResponse> candidates = new ArrayList<>();
        long id = 0;

        Double max = scorer.findDailyMax(graphs);

        for (DirectedAcyclicGraph<Node, DefaultWeightedEdge> graph : graphs) {
            List<NodeResponse> nodes = new ArrayList<>();
            List<EdgeResponse> edges = new ArrayList<>();
            id = 0;
            nodeIdMap.clear();

            for (Node node : graph.vertexSet()) {
                if (node instanceof DummyNode) {
                    continue;
                }

                String type = node instanceof EndpointNode ? "endpoint" : "parameter";
                nodes.add(new NodeResponse(id, node.getName(), type));

                nodeIdMap.put(node.getName(), id);
                id++;
            }

            for (DefaultWeightedEdge edge : graph.edgeSet()) {

                Node source = graph.getEdgeSource(edge);
                if (source instanceof DummyNode) {
                    continue;
                }

                Node target = graph.getEdgeTarget(edge);

                long from = nodeIdMap.get(source.getName());
                long to = nodeIdMap.get(target.getName());

                edges.add(new EdgeResponse(from, to));
            }

            candidates.add(new CandidateResponse(nodes, edges, scorer.score(graph, max)));


        }
        return candidates;
    }

    @GetMapping("/")
    public String get() {
        return "¯\\_(ツ)_/¯";
    }
}
