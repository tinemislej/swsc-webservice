package com.tinemislej.service.data;

import com.tinemislej.service.components.compose.PossibleSolutionFinder.CompositionSolution;

import java.util.List;

public class UnSuccessfulCompositionResponse {

    private final List<List<CompositionSolution>> solutions;

    public UnSuccessfulCompositionResponse(
            List<List<CompositionSolution>> solutions) {
        this.solutions = solutions;
    }
}
