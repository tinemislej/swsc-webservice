package com.tinemislej.service.data.response;

public class NodeResponse {

    private final long id;

    private final String name;

    private final String type;

    public NodeResponse(long id, String name, String type) {
        this.name = name;
        this.type = type;
        this.id = id;
    }


    public static final String TYPE_PARAMETER = "parameter";
    public static final String TYPE_ENDPOINT = "endpoint";
}
