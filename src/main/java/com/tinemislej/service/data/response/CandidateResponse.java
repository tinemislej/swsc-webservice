package com.tinemislej.service.data.response;

import java.util.List;

public class CandidateResponse {

    private final Double score;

    private final List<NodeResponse> nodes;

    private final List<EdgeResponse> edges;

    public CandidateResponse(List<NodeResponse> nodes, List<EdgeResponse> edges, Double score) {
        this.nodes = nodes;
        this.edges = edges;
        this.score = score;
    }

    public Double getScore() {
        return score;
    }
}
