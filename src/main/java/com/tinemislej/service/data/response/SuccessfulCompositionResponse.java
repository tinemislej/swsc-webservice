package com.tinemislej.service.data.response;

import java.util.List;

public class SuccessfulCompositionResponse {

    private final List<CandidateResponse> candidates;

    public SuccessfulCompositionResponse(List<CandidateResponse> candidates) {
        this.candidates = candidates;
    }
}
