package com.tinemislej.service.data.response;

public class EdgeResponse {

    private final long from;

    private final long to;

    public EdgeResponse(long from, long to) {
        this.from = from;
        this.to = to;
    }
}
