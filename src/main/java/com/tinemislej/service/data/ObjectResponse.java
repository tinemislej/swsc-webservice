package com.tinemislej.service.data;

public class ObjectResponse {

    private final Object body;

    public ObjectResponse(Object body) {
        this.body = body;
    }
}
