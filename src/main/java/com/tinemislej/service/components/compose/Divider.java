package com.tinemislej.service.components.compose;

import com.tinemislej.service.components.compose.model.DummyNode;
import com.tinemislej.service.components.compose.model.Node;
import com.tinemislej.service.components.compose.model.Node.NodeType;
import com.tinemislej.service.components.compose.model.ParameterNode;
import com.tinemislej.service.components.compose.model.SplitData;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class Divider {

    private final List<Node> lastNodes;

    private final DirectedAcyclicGraph<Node, DefaultWeightedEdge> graph;

    private final DirectedAcyclicGraph<Node, DefaultWeightedEdge> compositionCandidate;

    public Divider(List<Node> lastNodes,
            DirectedAcyclicGraph<Node, DefaultWeightedEdge> graph,
            DirectedAcyclicGraph<Node, DefaultWeightedEdge> compositionCandidate) {
        this.lastNodes = lastNodes;
        this.graph = graph;
        this.compositionCandidate = compositionCandidate;
    }

    public Divider(List<Node> lastNodes,
            DirectedAcyclicGraph<Node, DefaultWeightedEdge> graph) {
        this.lastNodes = lastNodes;
        this.graph = graph;
        this.compositionCandidate = new DirectedAcyclicGraph<>(DefaultWeightedEdge.class);
        for (Node n : lastNodes) {
            compositionCandidate.addVertex(n);
        }
    }

    @NonNull public List<SplitData> build(int dividerIdx) {
        List<Node> newLastNodes = new ArrayList<>();


        lastNodes.sort((o1, o2) -> {
            if (o1 instanceof ParameterNode && o2 instanceof ParameterNode) {
                boolean shouldSplit1 = shouldSplit((ParameterNode) o1, graph);
                boolean shouldSplit2 = shouldSplit((ParameterNode) o2, graph);

                if (shouldSplit1 == shouldSplit2) {
                    return 0;
                } else if (shouldSplit1) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                return 1;
            }
        });



        for (int i = 0; i < lastNodes.size(); i++) {
            Node node = lastNodes.get(i);
            if (node.getNodeType() == NodeType.ENDPOINT) {
                List<Node> inputs = getIncomingVertices(node, graph);

                for (Node input : inputs) {
                    if(!compositionCandidate.vertexSet().contains(input)) {
                        newLastNodes.add(input);
                    }
                    addNode(input, compositionCandidate);
                    compositionCandidate.addEdge(input, node);
                }
            } else if (node instanceof ParameterNode) {
                ParameterNode parameterNode = (ParameterNode) node;
                if (shouldSplit(parameterNode, graph)) {
                    List<Node> endpoints = getIncomingVertices(node, graph);

                    List<SplitData> splits  = split(parameterNode, endpoints, graph, compositionCandidate, newLastNodes, lastNodes.subList(i+1, lastNodes.size()));


                    Node endpoint = endpoints.get(0);

                    if (!newLastNodes.contains(endpoint) && !compositionCandidate.vertexSet().contains(endpoint)) {
                        newLastNodes.add(endpoint);
                    }

                    addNode(endpoint, compositionCandidate);
                    compositionCandidate.addEdge(endpoint, node);

                    newLastNodes.addAll(lastNodes.subList(i+1, lastNodes.size()));
                    lastNodes.clear();
                    lastNodes.addAll(newLastNodes);
                    return splits;
                } else {
                    List<Node> endpoints = getIncomingVertices(parameterNode, graph);
                    for (Node e : endpoints) {
                        if (!newLastNodes.contains(e) && !compositionCandidate.vertexSet().contains(e)) {
                            newLastNodes.add(e);
                        }

                        compositionCandidate.addVertex(e);
                        compositionCandidate.addEdge(e, parameterNode);

                    }
                }
            }
        }
        lastNodes.clear();
        lastNodes.addAll(newLastNodes);

        return Collections.emptyList();
    }

    private void addNode(Node n, DirectedAcyclicGraph<Node, DefaultWeightedEdge> g) {
        Set<Node> nodeSet = g.vertexSet();
        if (!nodeSet.contains(n)) {
            g.addVertex(n);
        }

    }

    public boolean isFinished() {
        return (lastNodes.size() == 1 && lastNodes.get(0) instanceof DummyNode) || lastNodes.size() == 0;
    }

    private List<SplitData> split(ParameterNode node, List<Node> endpoints,
            DirectedAcyclicGraph<Node, DefaultWeightedEdge> theGraph,
            DirectedAcyclicGraph<Node, DefaultWeightedEdge> candidate, List<Node> newLastNodes, List<Node> remainingNodes) {


        List<SplitData> splits = new ArrayList<>(endpoints.size());
        // create new dividers
        for (int i = 1; i < endpoints.size(); i++) {

            Node endpoint = endpoints.get(i);

            // add to last nodes
            List<Node> lastNodes = new ArrayList<>();
            lastNodes.add(endpoint);
            lastNodes.addAll(newLastNodes);
            lastNodes.addAll(remainingNodes);

            // clone
            DirectedAcyclicGraph<Node, DefaultWeightedEdge> split =
                    new DirectedAcyclicGraph<>(DefaultWeightedEdge.class);

            for (Node vertex : candidate.vertexSet()) {
                split.addVertex(vertex);
            }

            for (DefaultWeightedEdge e : candidate.edgeSet()) {
                split.addEdge(theGraph.getEdgeSource(e), theGraph.getEdgeTarget(e));
            }

            split.addVertex(endpoint);
            split.addEdge(endpoint, node);

            splits.add(new SplitData(lastNodes, split));

            //otherDividers.add(new Divider(lastNodes, theGraph, otherDividers, split));
        }

        return splits;
        //

    }

    public DirectedAcyclicGraph<Node, DefaultWeightedEdge> copy() {
        DirectedAcyclicGraph<Node, DefaultWeightedEdge> split =
                new DirectedAcyclicGraph<>(DefaultWeightedEdge.class);

        for (Node vertex : compositionCandidate.vertexSet()) {
            split.addVertex(vertex);
        }

        for (DefaultWeightedEdge e : compositionCandidate.edgeSet()) {
            split.addEdge(graph.getEdgeSource(e), graph.getEdgeTarget(e));
        }

        return split;
    }

    private List<Node> getIncomingVertices(Node n, DirectedAcyclicGraph<Node, DefaultWeightedEdge> g) {
        List<Node> vertices = new ArrayList<>();

        for (DefaultWeightedEdge e : g.incomingEdgesOf(n)) {
            vertices.add(g.getEdgeSource(e));
        }

        return vertices;
    }

    private boolean shouldSplit(ParameterNode node, DirectedAcyclicGraph<Node, DefaultWeightedEdge> graph) {
        List<Node> predecessors = getIncomingVertices(node, graph);
        return predecessors.size() >= 2;
//        if (predecessors.size() < 2) {
//            return false;
//        }
//
//        boolean doSplit = false;
//        for (int i = 0; i < predecessors.size(); i++) {
//            for (int j = 0; j < predecessors.size(); j++) {
//                if (i == j) {
//                    continue;
//                }
//
//                EndpointNode ni = (EndpointNode) predecessors.get(i);
//                EndpointNode nj = (EndpointNode) predecessors.get(i);
//
//                doSplit = doSplit | !ni.getName().equals(nj.getName());
//            }
//        }
//
//        return doSplit;
    }

    public DirectedAcyclicGraph<Node, DefaultWeightedEdge> getCandidate() {
        return compositionCandidate;
    }
}
