package com.tinemislej.service.components.compose.model;

public class DummyNode extends Node {

    private final NodeType nodeType;

    public DummyNode(String name, NodeType nodeType) {
        super(name);
        this.nodeType = nodeType;
    }

    @Override
    public NodeType getNodeType() {
        return nodeType;
    }
}
