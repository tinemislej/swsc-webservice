package com.tinemislej.service.components.compose.model;

import com.tinemislej.service.components.parse.endpoint.model.Concept;

public class ParameterNode extends Node {

    private final Concept concept;

    public ParameterNode(String name, Concept concept) {
        super(name);
        this.concept = concept;
    }

    @Override
    public NodeType getNodeType() {
        return NodeType.PARAMETER;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ParameterNode that = (ParameterNode) o;

        return concept != null ? concept.equals(that.concept) : that.concept == null;
    }

    @Override
    public int hashCode() {
        return concept != null ? concept.hashCode() : 0;
    }
}
