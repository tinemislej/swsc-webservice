package com.tinemislej.service.components.compose.model;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

public class GraphResult {

    private final boolean isSuccessful;

    private final DirectedAcyclicGraph<Node, DefaultWeightedEdge> left;

    private final DirectedAcyclicGraph<Node, DefaultWeightedEdge> right;

    public GraphResult(DirectedAcyclicGraph<Node, DefaultWeightedEdge> left,
            DirectedAcyclicGraph<Node, DefaultWeightedEdge> right) {
        this.isSuccessful = false;
        this.left = left;
        this.right = right;
    }

    public GraphResult(DirectedAcyclicGraph<Node, DefaultWeightedEdge> left) {
        this.isSuccessful = true;
        this.left = left;
        this.right = null;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public DirectedAcyclicGraph<Node, DefaultWeightedEdge> getLeft() {
        return left;
    }

    public DirectedAcyclicGraph<Node, DefaultWeightedEdge> getRight() {
        return right;
    }

    public DirectedAcyclicGraph<Node, DefaultWeightedEdge> getGraph() {
        if (!isSuccessful) {
            throw new IllegalStateException("result is not successful");
        }

        return left;
    }
}
