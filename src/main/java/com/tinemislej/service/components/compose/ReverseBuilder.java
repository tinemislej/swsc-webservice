package com.tinemislej.service.components.compose;

import com.tinemislej.service.components.compose.model.EndpointNode;
import com.tinemislej.service.components.compose.model.Node;
import com.tinemislej.service.components.compose.model.ParameterNode;
import com.tinemislej.service.components.match.Matcher;
import com.tinemislej.service.components.parse.endpoint.model.Concept;
import com.tinemislej.service.components.parse.endpoint.model.UiEndpoint;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class ReverseBuilder {

    private final Set<UiEndpoint> repository;
    private final Matcher matcher;

    public ReverseBuilder(Set<UiEndpoint> repo, Matcher matcher) {
        this.repository = repo;
        this.matcher = matcher;
    }

    public DirectedAcyclicGraph<Node, DefaultWeightedEdge> getGraph(Set<String> outs) {

        DirectedAcyclicGraph<Node, DefaultWeightedEdge> graph =
                new DirectedAcyclicGraph<>(DefaultWeightedEdge.class);

        // should end building graph
        boolean ended = false;

        // parameter main concept to node map
        HashMap<String, Node> parameterNodeMap = new HashMap<>();
        HashMap<String, Node> endpointNodeMap = new HashMap<>();

        Set<Concept> lastAdded = new HashSet<>();
        // put input concepts (parameters) as nodes in the graph
        Set<String> outputs = new HashSet<>();
        for (String s : outs) {
            outputs.add(s);
            outputs.addAll(matcher.getSubconcepts(s));
        }

        for(String s : outputs){
            Set<String> subconcpts = matcher.getSubconcepts(s);
            Concept outputConcept = new Concept(s, subconcpts);

            ParameterNode n = new ParameterNode(s, outputConcept);
            parameterNodeMap.put(outputConcept.getMainConcept(), n);
            lastAdded.add(outputConcept);
            graph.addVertex(n);
        }

//        for (String s : outs) {
//            Set<String> subconcpts = matcher.getSubconcepts(s);
//            Concept outputConcept = new Concept(s, subconcpts);
//
//            ParameterNode n = new ParameterNode(s, outputConcept);
//            parameterNodeMap.put(outputConcept.getMainConcept(), n);
//            lastAdded.add(outputConcept);
//            graph.addVertex(n);
//        }

        // endpoints already present in the graph
        Set<UiEndpoint> visitedEndpoints = new HashSet<>();

        while (!ended) {
            Set<Concept> concepts = new HashSet<>();
            for (Concept lastConcept : lastAdded) {
                for (UiEndpoint endpoint : repository) {
                    if (isConceptOutputOf(lastConcept, endpoint)) {
                        if (endpointNodeMap.getOrDefault(endpoint.getUrl(), null) == null) {
                            EndpointNode endpointNode = new EndpointNode(endpoint.getUrl());
                            graph.addVertex(endpointNode);
                            endpointNodeMap.put(endpoint.getUrl(), endpointNode);

                            graph.addEdge(endpointNode, parameterNodeMap.get(lastConcept.getMainConcept()));

                            for (Concept inputConcept : endpoint.getInputConcepts()) {
                                if (parameterNodeMap.getOrDefault(inputConcept.getMainConcept(), null) != null) {
                                    Node existingConcept = parameterNodeMap.get(inputConcept.getMainConcept());
                                    graph.addEdge(existingConcept, endpointNode);
                                } else {
                                    Node concept = new ParameterNode(inputConcept.getMainConcept(), inputConcept);
                                    graph.addVertex(concept);
                                    parameterNodeMap.put(inputConcept.getMainConcept(), concept);
                                    graph.addEdge(concept, endpointNode);
                                }
                                concepts.add(inputConcept);
                            }
                        } else {
                            Node existing = endpointNodeMap.get(endpoint.getUrl());
                            try {
                                graph.addEdge(existing, parameterNodeMap.get(lastConcept.getMainConcept()));
                                for (Concept inputConcept : endpoint.getInputConcepts()) {
                                    if (parameterNodeMap.getOrDefault(inputConcept.getMainConcept(), null) != null) {
                                        Node existingConcept = parameterNodeMap.get(inputConcept.getMainConcept());
                                        graph.addEdge(existingConcept, existing);
                                    } else {
                                        Node concept = new ParameterNode(inputConcept.getMainConcept(), inputConcept);
                                        parameterNodeMap.put(inputConcept.getMainConcept(), concept);
                                        graph.addVertex(concept);
                                        graph.addEdge(concept, existing);
                                    }
  //                                  concepts.add(inputConcept);
                                }
                            }catch(IllegalArgumentException ex){}
                        }

                    }
                }
            }

            lastAdded.clear();
            lastAdded.addAll(concepts);

            // check if we should end the composition
            // --> when we can no longer invoke any other api (apart from those already in the graph)  with available concepts
            ended = shouldEnd(lastAdded);
        }

        return graph;
    }

    private boolean shouldEnd(Set<Concept> lastAdded) {
        for (Concept c : lastAdded) {
            for (UiEndpoint e : repository) {
                if (isConceptOutputOf(c, e)) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isConceptOutputOf(Concept c, UiEndpoint e) {
        for (Concept outputConcept : e.getOutputConcepts()) {
            if (c.isSubconceptOf(outputConcept)) {
                return true;
            }
        }

        return false;
    }
}
