package com.tinemislej.service.components.compose;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntTools;
import com.hp.hpl.jena.ontology.OntTools.Path;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.util.iterator.Filter;
import com.tinemislej.service.components.compose.model.EndpointNode;
import com.tinemislej.service.components.compose.model.Node;
import com.tinemislej.service.components.compose.model.ParameterNode;
import com.tinemislej.service.components.match.Matcher;
import com.tinemislej.service.components.parse.endpoint.model.Concept;
import com.tinemislej.service.components.parse.endpoint.model.UiEndpoint;
import com.tinemislej.service.components.repo.EndpointRepo;
import com.tinemislej.service.utils.CollectionUtils;
import com.tinemislej.service.utils.GraphUtils;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.FileReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
public class PossibleSolutionFinder {

    private final String ONTOLOGY_URL = "/Users/tine/Desktop/tourismv3.owl";

    private final EndpointRepo repo;

    private final Matcher matcher;

    private OntModel ontModel = null;

    @Autowired
    public PossibleSolutionFinder(EndpointRepo repo, Matcher matcher) {
        this.repo = repo;
        this.matcher = matcher;
    }

    public List<List<CompositionSolution>> findPossibleSolutions(
            DirectedAcyclicGraph<Node, DefaultWeightedEdge> left,
            DirectedAcyclicGraph<Node, DefaultWeightedEdge> right,
            Set<String> outputConceptUrls) {

        //   Set<Concept> outputConcepts = new HashSet<>();
        Set<Concept> leftSideConcepts = new HashSet<>();
        Map<Concept, List<UiEndpoint>> endpointMap = new HashMap<>();
        List<Concept> outputConcepts = new ArrayList<>();

        // find left side concepts
        for (Node node : left.vertexSet()) {
            if (node instanceof ParameterNode) {
                leftSideConcepts.add(new Concept(node.getName(), matcher.getSubconcepts(node.getName())));
            }
        }

        // TODO remove if needed
//        Set<String> outs = new HashSet<>();
//        for (String s : outputConceptUrls) {
//            outs.add(s);
//            outs.addAll(matcher.getSubconcepts(s));
//        }
//
//        for (String s : outs) {
//            Concept c = new Concept(s, matcher.getSubconcepts(s));
//            if (!leftSideConcepts.contains(c)) {
//                outputConcepts.add(c);
//            }
//        }

        // parse outputs
        for (String s : outputConceptUrls) {
            outputConcepts.add(new Concept(s, matcher.getSubconcepts(s)));
        }

        // prepare endpoints map
        for (Concept c : outputConcepts) {
            List<UiEndpoint> eps = repo.getEndpointsThatReturnConcept(c);
            if (eps.size() > 0) {
                endpointMap.put(c, eps);
            }
        }

        // add right side endpoints to endpoints map
        for (Node node : right.vertexSet()) {
            if (node instanceof EndpointNode) {
                UiEndpoint e = repo.getEndpoint(node.getName());

                if (e != null) {
                    for (Concept c : e.getOutputConcepts()) {
                        List<UiEndpoint> eps = endpointMap.getOrDefault(c, null);
                        if (eps != null) {
                            eps.add(e);
                        } else {
                            List<UiEndpoint> initEps = new ArrayList<>();
                            initEps.add(e);
                            endpointMap.put(c, initEps);
                        }
                    }
                }
            }
        }

        List<Set<Concept>> solutions = new ArrayList<>();
        Set<Set<Concept>> solutionSet = new HashSet<>();

        // prepare possible solutions (minimal sets of concept that will satisfy the composition)
        Set<Concept> solution = new HashSet<>(outputConcepts);
        solutions.add(solution);
        solutionSet.add(solution);

        for (int i = 0; i < solutions.size(); i++) {
            Set<Concept> previous = solutions.get(i);

            for (Concept concept : previous) {
                List<UiEndpoint> conceptEndpoints = endpointMap.getOrDefault(concept, null);
                if (conceptEndpoints != null) {
                    for (UiEndpoint e : conceptEndpoints) {
                        Set<Concept> newSolutionConcepts = new HashSet<>(previous); // clone
                        newSolutionConcepts.remove(concept);
                        newSolutionConcepts.addAll(e.getInputConcepts());
                        Set<Concept> s = new HashSet<>(newSolutionConcepts);
                        if(!solutionSet.contains(s)) {
                            solutions.add(s);
                            solutionSet.add(s);
                        }
                    }
                }
            }
        }

        Set<CompositionSolution> compositionSolutionSet = new HashSet<>();

        // prepare pairings from solution sets
        // all pairings
        for (Set<Concept> ps : solutionSet) {
            List<List<Pairing>> combos = new ArrayList<>();
            for (Concept c : ps) {
                List<Pairing> pairings = new ArrayList<>();
                if (leftSideConcepts.contains(c) || GraphUtils.containsSubConcept(leftSideConcepts, c)) {
                    continue;
                } else {
                    for (Concept leftConcept : leftSideConcepts) {
                        Optional<Integer> dist1 = getDistance(leftConcept.getMainConcept(), c.getMainConcept());
                        Optional<Integer> dist2 = getDistance(c.getMainConcept(), leftConcept.getMainConcept());

                        Integer distance = null;
                        if (dist1.isPresent() & dist2.isPresent()) {
                            distance = Math.min(dist1.get(), dist2.get());
                        } else if (dist1.isPresent()) {
                            distance = dist1.get();
                        } else if (dist2.isPresent()) {
                            distance = dist2.get();
                        }

                        pairings.add(new Pairing(leftConcept, c, distance));
                    }
                    combos.add(pairings);
                }

            }
            List<List<Pairing>> cmbs = CollectionUtils.getCombos(combos);
            for (List<Pairing> pairs : cmbs) {
                compositionSolutionSet.add(new CompositionSolution(pairs));
            }
        }

        // maximum

//        for (Set<Concept> ps : solutionSet) {
//            List<Pairing> pairings = new ArrayList<>();
//            for (Concept c : ps) {
//                if (leftSideConcepts.contains(c)) {
//                    continue;
//                } else {
//                    Integer score = null;
//                    Pairing p = null;
//                    for (Concept leftConcept : leftSideConcepts) {
//                        Optional<Integer> dist1 = getDistance(c.getMainConcept(), leftConcept.getMainConcept());
//                        Optional<Integer> dist2 = getDistance(leftConcept.getMainConcept(), c.getMainConcept());
//
//                        if (dist1.isPresent() & dist2.isPresent()) {
//                            int score1 = dist1.get();
//                            int score2 = dist2.get();
//                            int currentscore = Math.min(score1, score2);
//                            if (score == null || currentscore < score) {
//                                score = currentscore;
//                                p = new Pairing(leftConcept, c, score);
//                            }
//
//                        } else if (dist1.isPresent()) {
//                            if (score == null || dist1.get() < score) {
//                                score = dist1.get();
//                                p = new Pairing(leftConcept, c, score);
//                            }
//                        } else if (dist2.isPresent()) {
//                            if (score == null || dist2.get() < score) {
//                                score = dist2.get();
//                                p = new Pairing(leftConcept, c, score);
//                            }
//                        } else if (p == null) {
//                            p = new Pairing(leftConcept, c, Integer.MAX_VALUE);
//                        }
//                    }
//
//                    pairings.add(p);
//                }
//            }
//
//            compositionSolutionSet.add(new CompositionSolution(pairings));
//        }

        List<CompositionSolution> compositionSolutions = new ArrayList<>(compositionSolutionSet);

        return groupSolutions(compositionSolutions);
    }


    private Optional<Integer> getDistance(String thisConcept, String thatConcept) {
        try {
            URI u = URI.create(thisConcept);
            String ns = String.format("%s://%s", u.getScheme(), u.getHost());

            if(ontModel == null) {
                 ontModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
                ontModel.read(new FileReader(ONTOLOGY_URL), "");
            }

            OntClass from = ontModel.getOntClass(thisConcept);
            OntClass to = ontModel.getOntClass(thatConcept);
            if (from == null || to == null) {
                return Optional.empty();
            }
            Path path = OntTools.findShortestPath(ontModel, from, to, FILTER);

            if (path == null) {
                return Optional.empty();
            } else {
                int n = 0;
                for (Statement s : path) {
                    if (s.getObject().toString().startsWith(ns)) {
                        // filter out OWL Classes
                        n++;
                    }
                }
                return Optional.of(n);
            }
        } catch (Exception ex) {
            return Optional.empty();
        }
    }

    public static class CompositionSolution {
        private final List<Pairing> pairings;

        public CompositionSolution(List<Pairing> pairings) {
            this.pairings = pairings;
        }

        @Override public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CompositionSolution that = (CompositionSolution) o;

            return pairings != null ? pairings.equals(that.pairings) : that.pairings == null;
        }

        @Override public int hashCode() {
            return pairings != null ? pairings.hashCode() : 0;
        }
    }

    public static class Pairing {
        private final Concept from;
        private final Concept to;
        private final Integer distance;

        public Pairing(Concept from, Concept to, Integer distance) {
            this.from = from;
            this.to = to;
            this.distance = distance;
        }

        public Concept getFrom() {
            return from;
        }

        public Concept getTo() {
            return to;
        }

        public Integer getDistance() {
            return distance;
        }

        @Override public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pairing pairing = (Pairing) o;

            if (from != null ? !from.equals(pairing.from) : pairing.from != null) return false;
            if (to != null ? !to.equals(pairing.to) : pairing.to != null) return false;
            return distance != null ? distance.equals(pairing.distance) : pairing.distance == null;
        }

        @Override public int hashCode() {
            int result = from != null ? from.hashCode() : 0;
            result = 31 * result + (to != null ? to.hashCode() : 0);
            result = 31 * result + (distance != null ? distance.hashCode() : 0);
            return result;
        }
    }

    private final Filter<Statement> FILTER = new Filter<Statement>() {
        @Override public boolean accept(Statement o) {
            String predicate = o.getPredicate().getLocalName();
            return predicate.equals("subClassOf") || predicate.equals("subPropertyOf");
        }
    };


    private List<List<CompositionSolution>> groupSolutions(List<CompositionSolution> solutions) {
        List<List<CompositionSolution>> bestSolutions = new ArrayList<>();

        Map<Integer, List<CompositionSolution>> groups = new HashMap<>();
        for (CompositionSolution solution : solutions) {
            int size = solution.pairings.size();
            List<CompositionSolution> group = groups.getOrDefault(size, null);
            if (group == null) {
                group = new ArrayList<>();
                group.add(solution);
                groups.put(size, group);
            } else {
                group.add(solution);
            }
        }

        for (Integer key : groups.keySet()) {
            List<CompositionSolution> group = groups.get(key);
            Collections.sort(group, (o1, o2) -> {
                List<Pairing> pairings1 = o1.pairings;
                List<Pairing> pairings2 = o2.pairings;

                int inf1 = 0;
                int inf2 = 0;
                int dist1 = 0;
                int dist2 = 0;

                for (Pairing p : pairings1) {
                    if (p.distance == null || p.distance == Integer.MAX_VALUE) {
                        inf1++;
                    } else {
                        dist1 += p.distance;
                    }
                }

                for (Pairing p : pairings2) {
                    if (p.distance == null || p.distance == Integer.MAX_VALUE) {
                        inf2++;
                    } else {
                        dist2 += p.distance;
                    }
                }

                if (inf1 != inf2) {
                    return Integer.compare(inf1, inf2);
                } else {
                    return Integer.compare(dist1, dist2);
                }

            });

            if (group.size() >= 2) {
                bestSolutions.add(group.subList(0, 2));
            } else {
                bestSolutions.add(group.subList(0, 1));
            }


        }
        return bestSolutions;
    }
}
