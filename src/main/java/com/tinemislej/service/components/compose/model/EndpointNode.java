package com.tinemislej.service.components.compose.model;

public class EndpointNode extends Node {

    public EndpointNode(String name) {
        super(name);
    }

    @Override
    public NodeType getNodeType() {
        return NodeType.ENDPOINT;
    }
}
