package com.tinemislej.service.components.compose;

import com.tinemislej.service.components.compose.model.DummyNode;
import com.tinemislej.service.components.compose.model.EndpointNode;
import com.tinemislej.service.components.compose.model.GraphResult;
import com.tinemislej.service.components.compose.model.Node;
import com.tinemislej.service.components.compose.model.ParameterNode;
import com.tinemislej.service.components.match.Matcher;
import com.tinemislej.service.components.parse.endpoint.model.Concept;
import com.tinemislej.service.components.parse.endpoint.model.UiEndpoint;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LayerComposer {

    private final Set<UiEndpoint> repository;
    private final Matcher matcher;

    public LayerComposer(Set<UiEndpoint> repo, Matcher matcher) {
        this.repository = repo;
        this.matcher = matcher;
    }

    public GraphResult getGraph(List<String> inputConcepts, Set<String> outputConceptUrls) {

        DirectedAcyclicGraph<Node, DefaultWeightedEdge> graph =
                new DirectedAcyclicGraph<>(DefaultWeightedEdge.class);

        // should end building graph
        boolean ended = false;

        // union of input and ouput concepts so far
        Set<Concept> availableConcepts = new HashSet<>();
        Set<Concept> outputs = new HashSet<>();

        for (String s : outputConceptUrls) {
            outputs.add(new Concept(s, matcher.getSubconcepts(s)));
        }

        // parameter main concept to node map
        HashMap<String, Node> parameterNodeMap = new HashMap<>();

        // put input concepts (parameters) as nodes in the graph
        for (String s : inputConcepts) {
            Set<String> subconcpts = matcher.getSubconcepts(s);
            Concept inputConcept = new Concept(s, subconcpts);
            availableConcepts.add(inputConcept);

            ParameterNode n = new ParameterNode(s, inputConcept);
            parameterNodeMap.put(inputConcept.getMainConcept(), n);
            graph.addVertex(n);
        }

        // endpoints already present in the graph
        Set<UiEndpoint> visitedEndpoints = new HashSet<>();
        while (!ended) {

            // output concepts of services that were added in this layer (iteration)
            Set<Concept> outputConcepts = new HashSet<>();

            for (UiEndpoint endpoint : repository) {

                if (visitedEndpoints.contains(endpoint)) {
                    continue;
                }

                // a set of concepts (parameters) that will be used to call the endpoint
                // if null, at least one of the parameters is missing and therefore endpoint can't be called
                Set<Concept> invocationConcepts = endpoint.canCallWith(availableConcepts);
                if (invocationConcepts != null) { // we are able to call the endpoint
                    EndpointNode endpointNode = new EndpointNode(endpoint.getUrl());
                    graph.addVertex(endpointNode);
                    for (Concept c : invocationConcepts) {
                        // add invocation concepts to graph (if not already) and connect them with endpoint node

                        Node n = parameterNodeMap.getOrDefault(c.getMainConcept(), null);
                        if (n != null) {
                            graph.addEdge(n, endpointNode);
                        } else {
                            Node newConcept = new ParameterNode(c.getMainConcept(), c);
                            parameterNodeMap.put(c.getMainConcept(), newConcept);
                            graph.addVertex(newConcept);
                            graph.addEdge(newConcept, endpointNode);
                        }
                    }

                    // add output concepts to the set (they will be used in the next layer/iteration) m
                    outputConcepts.addAll(endpoint.getOutputConcepts());

                    for (Concept c : endpoint.getOutputConcepts()) {
                        if (endpoint.getInputConcepts().contains(c)) {
                            continue; // no self-loops
                        }
                        // add output concepts to graph (if not already) and connect them with endpoint node
                        Node n = parameterNodeMap.getOrDefault(c.getMainConcept(), null);
                        if (n != null) {
                            try {
                                graph.addEdge(endpointNode, n);
                            } catch (IllegalArgumentException ex) {
                            }
                        } else {
                            Node newConcept = new ParameterNode(c.getMainConcept(), c);
                            parameterNodeMap.put(c.getMainConcept(), newConcept);
                            graph.addVertex(newConcept);
                            graph.addEdge(endpointNode, newConcept);
                        }
                    }

                    visitedEndpoints.add(endpoint); // 'mark' endpoint as visited.
                }
            }

            // add output concepts to concepts set, so they can be used in following iteration as input concepts


            availableConcepts.addAll(outputConcepts);

            // check if we should end the composition
            // --> when we can no longer invoke any other api (apart from those already in the graph)  with available concepts
            ended = shouldEnd(availableConcepts, visitedEndpoints);
        }

        boolean success = hasEndedSuccessfully(availableConcepts, outputs);
        if (success) {
            return new GraphResult(graph);
        } else {
            DirectedAcyclicGraph<Node, DefaultWeightedEdge> reverse
                    = new ReverseBuilder(repository, matcher).getGraph(outputConceptUrls);
            return new GraphResult(graph, reverse);
        }

    }

    private boolean hasEndedSuccessfully(Set<Concept> availableConcepts, Set<Concept> requiredConcepts) {
        int i = 0;
        for (Concept c : availableConcepts) {
            for (Concept r : requiredConcepts) {
                if (r.getMainConcept().equals(c.getMainConcept()) || c.isSubconceptOf(r)) {
                    i++;
                }
                if (i == requiredConcepts.size()) {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean shouldEnd(Set<Concept> concepts, Set<UiEndpoint> visited) {
        for (UiEndpoint endpoint : repository) {
            if (!visited.contains(endpoint)) {
                boolean canContinue = endpoint.canCallWith(concepts) != null;
                if (canContinue) {
                    return false;
                }
            }
        }

        return true;
    }

    public DirectedAcyclicGraph<Node, DefaultWeightedEdge> addDummyNodes(
            DirectedAcyclicGraph<Node, DefaultWeightedEdge> graph,
            Set<String> inputConcepts, Set<String> outputConcepts) {
        DirectedAcyclicGraph<Node, DefaultWeightedEdge> g =
                (DirectedAcyclicGraph<Node, DefaultWeightedEdge>) graph.clone();


        // we could optimize this by remembering input and output nodes at composition time

        Set<Node> inputNodes = new HashSet<>();
        Set<Node> outputNodes = new HashSet<>();

        for (Node n : g.vertexSet()) {
            if (n.getNodeType() == Node.NodeType.PARAMETER) {
                if (inputConcepts.contains(n.getName())) {
                    inputNodes.add(n);
                }

                for (String c : outputConcepts) { // TODO
                    if (c.equals(n.getName()) || matcher.getSubconcepts(c).contains(n.getName())) {
                        outputNodes.add(n);
                    }
                }
//                if (outputConcepts.contains(n.getName()) || CollectionUtils.containsAny(outputConcepts, matcher.getSubconcepts(n.getName()))) {
//                    outputNodes.add(n);
//                }
            }
        }

        DummyNode startNode = new DummyNode("START", Node.NodeType.START);
        DummyNode endNode = new DummyNode("END", Node.NodeType.END);

        g.addVertex(startNode);
        g.addVertex(endNode);

        for (Node n : inputNodes) {
            g.addEdge(startNode, n);
        }

        for (Node n : outputNodes) {
            g.addEdge(n, endNode);
        }


        return g;
    }
}