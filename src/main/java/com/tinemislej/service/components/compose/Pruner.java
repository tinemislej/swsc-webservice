package com.tinemislej.service.components.compose;

import com.tinemislej.service.components.compose.model.Node;
import com.tinemislej.service.components.compose.model.Node.NodeType;
import com.tinemislej.service.components.compose.model.ParameterNode;
import com.tinemislej.service.components.parse.api.model.Parameter;

import org.jgrapht.graph.AsUndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.jgrapht.traverse.BreadthFirstIterator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class Pruner {

    public DirectedAcyclicGraph<Node, DefaultWeightedEdge> pruneGraph(DirectedAcyclicGraph<Node, DefaultWeightedEdge> graph) {

        AsUndirectedGraph<Node, DefaultEdge> undirectedGraph = new AsUndirectedGraph(graph);
        DirectedAcyclicGraph<Node, DefaultWeightedEdge> pruned =
                (DirectedAcyclicGraph<Node, DefaultWeightedEdge>) graph.clone();
        boolean continuePruning;
        do {
            BreadthFirstIterator<Node, DefaultEdge> iterator = new BreadthFirstIterator<>(undirectedGraph);
            boolean shouldContinue = false;
            while (iterator.hasNext()) {
                Node n = iterator.next();
                if (n.getNodeType() == Node.NodeType.ENDPOINT) {
                    if (shouldBeMarkedForDeletion(n, pruned)) {
                        shouldContinue = true;

                        List<DefaultWeightedEdge> paramEdges = new ArrayList<>(graph.outgoingEdgesOf(n));
                        for (int i = 0; i < paramEdges.size(); i++) {
                            Node target = pruned.getEdgeTarget(paramEdges.get(i));
                            //pruned.removeVertex(target);
                            undirectedGraph.removeVertex(target);
//                            try {
//                                undirectedGraph.removeVertex(target);
//                            }catch (Exception ex){
//                                ex.printStackTrace();
//                            }
                        }
                        undirectedGraph.removeVertex(n);

//                        try {
//                            pruned.removeVertex(n);
//                            undirectedGraph.removeVertex(n);
//                        }catch (Exception ex){}

                        break;
                    }
                }
            }
            continuePruning = shouldContinue;
        } while (continuePruning);


        return pruned;
    }

    private boolean shouldBeMarkedForDeletion(Node n, DirectedAcyclicGraph<Node, DefaultWeightedEdge> graph) {
        if (n.getNodeType() == Node.NodeType.ENDPOINT) {
            Set<DefaultWeightedEdge> outs = graph.outgoingEdgesOf(n);
            for (DefaultWeightedEdge e : outs) {
                Node paramNode = graph.getEdgeTarget(e);
                if (graph.outgoingEdgesOf(paramNode).size() > 0) {
                    return false;
                }
            }
            return true; // none of params has connections
        }

        return false;
    }
}
