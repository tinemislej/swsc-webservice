package com.tinemislej.service.components.compose.model;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;

import java.util.List;

public class SplitData {

    private final List<Node> lastNodes;

    private final DirectedAcyclicGraph<Node, DefaultWeightedEdge> graph;

    public SplitData(List<Node> lastNodes,
            DirectedAcyclicGraph<Node, DefaultWeightedEdge> graph) {
        this.lastNodes = lastNodes;
        this.graph = graph;
    }

    public List<Node> getLastNodes() {
        return lastNodes;
    }

    public DirectedAcyclicGraph<Node, DefaultWeightedEdge> getGraph() {
        return graph;
    }
}
