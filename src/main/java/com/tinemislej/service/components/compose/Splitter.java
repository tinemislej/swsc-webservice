package com.tinemislej.service.components.compose;

import com.tinemislej.service.components.compose.model.Node;
import com.tinemislej.service.components.compose.model.SplitData;
import com.tinemislej.service.components.match.Matcher;
import com.tinemislej.service.utils.CollectionUtils;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

@Service
public class Splitter {

    private final Matcher matcher;

    @Autowired
    public Splitter(Matcher matcher) {
        this.matcher = matcher;
    }

    public List<DirectedAcyclicGraph<Node, DefaultWeightedEdge>> split(
            DirectedAcyclicGraph<Node, DefaultWeightedEdge> g, List<String> outputs) {
        List<Node> endNodes = getIncomingVertices(getEndNode(g), g);
        List<Divider> dividers = new ArrayList<>();
        List<List<Node>> ends = getEndings(endNodes, outputs);
        for (List<Node> endings : ends) {
            dividers.add(new Divider(endings, g));
        }
        //Divider initial = new Divider(endNodes, g);
        Set<Integer> dones = new HashSet<>();
        // dividers.add(initial);

        List<DirectedAcyclicGraph<Node, DefaultWeightedEdge>> graphs = new ArrayList<>();

        while (dones.size() < dividers.size()) {
            for (int i = 0; i < dividers.size(); i++) {
                if (dones.contains(i)) {
                    continue;
                }

                if (dividers.get(i).isFinished()) {
                    dones.add(i);
                    graphs.add(dividers.get(i).getCandidate());

                } else {
                    List<SplitData> splits = dividers.get(i).build(i);
                    if (splits.size() > 0) {
                        for (SplitData data : splits) {
                            dividers.add(new Divider(data.getLastNodes(), g, data.getGraph()));
                        }
                        break;
                    }
                }

            }
        }

        return graphs;
    }

    private List<Node> getIncomingVertices(Node n, DirectedAcyclicGraph<Node, DefaultWeightedEdge> g) {
        List<Node> vertices = new ArrayList<>();

        for (DefaultWeightedEdge e : g.incomingEdgesOf(n)) {
            vertices.add(g.getEdgeSource(e));
        }

        return vertices;
    }

    private List<List<Node>> getEndings(List<Node> endNodes, List<String> outputs) {
        List<List<Node>> allBuckets = new ArrayList<>();
        for (String q : outputs) {
            Set<String> subconcepts = matcher.getSubconcepts(q);
            Set<String> allConcepts = new HashSet<>();

            if (subconcepts != null) {
                allConcepts.addAll(subconcepts);
            }
            allConcepts.add(q);

            List<Node> bucket = new ArrayList<>();

            for (Node n : endNodes) {
                if (allConcepts.contains(n.getName())) {
                    bucket.add(n);
                }
            }

            allBuckets.add(bucket);
        }

        System.out.println();
        return CollectionUtils.getCombos(allBuckets);
    }

    private Node getEndNode(DirectedAcyclicGraph<Node, DefaultWeightedEdge> g) {
        for (Node n : g.vertexSet()) {
            if (n.getNodeType() == Node.NodeType.END) {
                return n;
            }
        }

        throw new NoSuchElementException("cant find end node");
    }


}
