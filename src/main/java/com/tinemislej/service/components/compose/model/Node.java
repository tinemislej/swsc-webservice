package com.tinemislej.service.components.compose.model;

public abstract class Node {

    private final String name;

    public Node(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract NodeType getNodeType();

    public enum NodeType {
        PARAMETER,
        ENDPOINT,
        START,
        END
    }
}
