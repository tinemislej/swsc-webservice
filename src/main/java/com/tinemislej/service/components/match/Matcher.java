package com.tinemislej.service.components.match;


import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.tinemislej.service.components.parse.api.model.Endpoint;
import com.tinemislej.service.components.parse.api.model.Parameter;
import com.tinemislej.service.components.parse.api.model.Response;
import com.tinemislej.service.components.parse.api.model.ResponseEntity;


@Service
public class Matcher {

    private static final String ENV_SPARQL = "sparql.host";
    @Autowired Environment env;

    public boolean match(Endpoint input, Endpoint output) {
        for (Parameter p : output.getInputParameters()) {
            if (!matchParameter(p, input)) {
                return false;
            }
        }

        return true;
    }

    private boolean matchParameter(Parameter p, Endpoint input) {
        for (Response response : input.getResponses()) {
            if (!response.getCode().startsWith("2")) {
                continue;
            }
            List<ResponseEntity> inputResponse = response.getEntities();
            for (ResponseEntity r : inputResponse) {
                if (matches(r, p)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean matches(ResponseEntity r, Parameter p) {
        if (p.getTypeUrl() == null || p.getTypeUrl().length == 0) {
            return true;
        }
        for (String responseType : r.getTypeUrl()) {
            for (String inputType : p.getTypeUrl()) {
                if (matches(inputType, responseType)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean matches(String inputType, String outputType) {
        return inputType.equals(outputType) || isSubclassOf(outputType, inputType);
    }

    public Set<String> getSubconcepts(String concept) {
        String query = String.format("prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                + "SELECT DISTINCT ?class\n"
                + "WHERE  { { ?class rdfs:subClassOf+ <%s> } UNION { ?class rdfs:subPropertyOf+ "
                + "<%s> } }\n", concept, concept);

        String sparqlEndpoint = env.getProperty(ENV_SPARQL);
        if(sparqlEndpoint == null){
            throw new IllegalStateException("SPARQL env. variable not set");
        }
        QueryExecution queryExec = QueryExecutionFactory.sparqlService(sparqlEndpoint, query);

        //QueryExecution queryExec = QueryExecutionFactory.sparqlService("http://localhost:3030/tourismv3", query);

        ResultSet resultSet = queryExec.execSelect();

        Set<String> subs = new HashSet<>();
        while (resultSet.hasNext()) {
            QuerySolution solution = resultSet.nextSolution();

            subs.add(solution.get("class").asResource().getURI());
        }

        return subs;
    }

    public boolean isSubclassOf(String thiz, String that) {
        String query = String.format("prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                + "SELECT DISTINCT ?class\n"
                + "WHERE  { { ?class rdfs:subClassOf+ <%s> } UNION { ?class rdfs:subPropertyOf+ "
                + "<%s> } }\n", thiz, thiz);

        String sparqlEndpoint = env.getProperty(ENV_SPARQL);
        if(sparqlEndpoint == null){
            throw new IllegalStateException("SPARQL env. variable not set");
        }
        QueryExecution queryExec = QueryExecutionFactory.sparqlService(sparqlEndpoint, query);
        //QueryExecution queryExec = QueryExecutionFactory.sparqlService("http://localhost:3030/tourismv3", query);
        ResultSet resultSet = queryExec.execSelect();

        while (resultSet.hasNext()) {
            QuerySolution solution = resultSet.nextSolution();

            if (solution.get("class").asResource().getURI().equals(that)) {
                return true;
            }
        }
        return false;
    }


}
