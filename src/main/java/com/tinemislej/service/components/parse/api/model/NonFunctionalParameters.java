package com.tinemislej.service.components.parse.api.model;

import java.util.ArrayList;
import java.util.List;

public class NonFunctionalParameters {

    private final List<NonFunctionalParameter> parameters;

    public NonFunctionalParameters() {
        this.parameters = new ArrayList<>();
    }

    public NonFunctionalParameters(
            List<NonFunctionalParameter> parameters) {
        this.parameters = parameters;
    }

    public void addParameter(NonFunctionalParameter parameter) {
        this.parameters.add(parameter);
    }

    public List<NonFunctionalParameter> getParameters() {
        return parameters;
    }
}
