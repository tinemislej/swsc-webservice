package com.tinemislej.service.components.parse.api;

import com.github.jsonldjava.core.JsonLdError;
import com.tinemislej.service.components.parse.api.model.Endpoint;

import java.io.IOException;
import java.util.List;


public interface ApiParser {

    List<Endpoint> parseApiDefinition(String source) throws JsonLdError, IOException, JsonLdError;
}