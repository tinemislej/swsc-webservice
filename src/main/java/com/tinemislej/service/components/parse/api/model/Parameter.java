package com.tinemislej.service.components.parse.api.model;

public class Parameter extends BaseParameter {

    private final boolean required;

    public Parameter(String[] types, boolean required) {
        super(types);
        this.required = required;
    }

    public boolean isRequired() {
        return required;
    }
}
