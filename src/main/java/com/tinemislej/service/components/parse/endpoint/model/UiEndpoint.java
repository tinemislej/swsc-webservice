package com.tinemislej.service.components.parse.endpoint.model;

import com.tinemislej.service.components.parse.api.model.NonFunctionalParameters;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nullable;

public class UiEndpoint {

    private final String url;

    private final Set<Concept> inputConcepts;

    private final Set<Concept> outputConcepts;

    @Nullable private final NonFunctionalParameters nonFunctionalParameters;

    public UiEndpoint(String name, Set<Concept> inputConcepts, Set<Concept> outputConcepts,
            @Nullable NonFunctionalParameters nonFunctionalParameters) {
        this.url = name;
        this.inputConcepts = inputConcepts;
        this.outputConcepts = outputConcepts;
        this.nonFunctionalParameters = nonFunctionalParameters;
    }

    public UiEndpoint(String name, Set<Concept> inputConcepts, Set<Concept> outputConcepts) {
        this(name, inputConcepts, outputConcepts, null);
    }

    public String getUrl() {
        return url;
    }

    public Set<Concept> getInputConcepts() {
        return inputConcepts;
    }

    public Set<Concept> getOutputConcepts() {
        return outputConcepts;
    }

    @Nullable public Set<Concept> canCallWith(Set<Concept> concepts) {
        Set<Concept> matched = new HashSet<>();
        for (Concept inputConcept : this.inputConcepts) {
            for (Concept c : concepts) {
                if (c.isSubconceptOf(inputConcept)) {
                    matched.add(c);
                }
            }
        }

        if (matched.size() >= inputConcepts.size()) {
            return matched;
        } else {
            return null;
        }
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UiEndpoint that = (UiEndpoint) o;

        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        if (inputConcepts != null ? !inputConcepts.equals(that.inputConcepts) : that.inputConcepts != null)
            return false;
        if (outputConcepts != null ? !outputConcepts.equals(that.outputConcepts) : that.outputConcepts != null)
            return false;
        return nonFunctionalParameters != null ? nonFunctionalParameters.equals(
                that.nonFunctionalParameters) : that.nonFunctionalParameters == null;
    }

    @Nullable public NonFunctionalParameters getNonFunctionalParameters() {
        return nonFunctionalParameters;
    }

    @Override
    public int hashCode() {
        return url != null ? url.hashCode() : 0;
    }
}
