package com.tinemislej.service.components.parse.reference;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import me.andrz.jackson.JsonReferenceException;
import me.andrz.jackson.JsonReferenceProcessor;
import me.andrz.jackson.ObjectMapperFactory;

public class ReferenceProcessor {

    private final JsonReferenceProcessor referenceProcessor;
    private final ObjectMapper mapper;

    public ReferenceProcessor() {
        referenceProcessor = new JsonReferenceProcessor();
        referenceProcessor.setStopOnCircular(false); // default true

        referenceProcessor.setMaxDepth(-1);
        referenceProcessor.setMapperFactory(new ObjectMapperFactory() {
            @Override
            public ObjectMapper create(URL url) {
                //ObjectMapper objectMapper = DefaultObjectMapperFactory.instance.create(url);
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
                return objectMapper;
            }
        });

        mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, false);

    }

    public String process(File file) throws IOException, JsonReferenceException {
        JsonNode node = referenceProcessor.process(file);
        return mapper.writeValueAsString(node);
    }
}
