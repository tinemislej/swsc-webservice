package com.tinemislej.service.components.parse.api;

import com.github.jsonldjava.core.JsonLdConsts;
import com.github.jsonldjava.core.JsonLdError;
import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.tinemislej.service.components.parse.api.model.NonFunctionalParameters;
import com.tinemislej.service.components.parse.api.model.ResponseLimitNonFunctionalParameter;
import com.tinemislej.service.components.parse.api.model.ResponseLimitParameter;
import com.tinemislej.service.components.parse.api.model.StringNonFunctionalParameter;
import com.tinemislej.service.components.parse.api.model.Endpoint;
import com.tinemislej.service.components.parse.api.model.Parameter;
import com.tinemislej.service.components.parse.api.model.ResponseEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import io.swagger.models.HttpMethod;
import io.swagger.models.Model;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.Response;
import io.swagger.models.Swagger;
import io.swagger.models.parameters.BodyParameter;
import io.swagger.models.properties.ArrayProperty;
import io.swagger.models.properties.ObjectProperty;
import io.swagger.models.properties.Property;
import io.swagger.models.properties.RefProperty;
import io.swagger.parser.Swagger20Parser;

/**
 * ApiParser will be used for parsing a source (swagger.json file)
 **/
public class ApiParserImpl implements ApiParser {

    private static final String XSEMANTICS = "x-semantics";
    private final Swagger20Parser parser;

    public ApiParserImpl() {
        parser = new Swagger20Parser();
    }

    @Override
    public List<Endpoint> parseApiDefinition(String source) throws JsonLdError, IOException {
        Swagger swagger = parser.parse(source);
        List<Endpoint> endpoints = new ArrayList<>();

        Map<String, Path> paths = swagger.getPaths();
        for (String pathKey : paths.keySet()) {
            Path path = paths.get(pathKey);
            Map<HttpMethod, Operation> operationsMap = path.getOperationMap();


            for (HttpMethod httpMethod : operationsMap.keySet()) {
                Endpoint.HttpMethod method = getHttpMethodFromName(httpMethod.name());
                Operation operation = operationsMap.get(httpMethod);

                List<io.swagger.models.parameters.Parameter> params = operation.getParameters();

                Parameter[] operationParameters = getInputParameters(params, swagger);

                List<com.tinemislej.service.components.parse.api.model.Response> output =
                        getOutput(swagger, operationsMap.get(httpMethod).getResponses());
                String url = String.format("%s%s%s", swagger.getHost(), swagger.getBasePath(), pathKey);

                Optional<NonFunctionalParameters> nonFunctionalParameters = parseNonFunctionalParameters(
                        operation.getVendorExtensions());
                Endpoint endpoint = new Endpoint.Builder()
                        .withHttpMethod(method)
                        .withInputParameters(operationParameters)
                        .withResponses(output)
                        .withNonFunctionalParameters(nonFunctionalParameters.orElse(null))
                        .withUrl(url)
                        .build();

                endpoints.add(endpoint);
            }
        }

        return endpoints;
    }

    private Endpoint.HttpMethod getHttpMethodFromName(String name) {
        switch (name.toLowerCase()) {
            case "get":
                return Endpoint.HttpMethod.GET;
            case "post":
                return Endpoint.HttpMethod.POST;
            case "patch":
                return Endpoint.HttpMethod.PATCH;
            case "put":
                return Endpoint.HttpMethod.PUT;
            case "delete":
                return Endpoint.HttpMethod.DELETE;
            default:
                throw new IllegalStateException("method not supported");
        }
    }


    private Parameter[] getInputParameters(List<io.swagger.models.parameters.Parameter> operationparameters, Swagger
            swagger) throws JsonLdError {

        List<Parameter> inputParameters = new ArrayList<>();

        for (io.swagger.models.parameters.Parameter param : operationparameters) {

            List<Parameter> parameter = parseInputParameter(param, swagger);

            inputParameters.addAll(parameter);
        }

        return inputParameters.toArray(new Parameter[inputParameters.size()]);
    }

    public List<com.tinemislej.service.components.parse.api.model.Response> getOutput(Swagger swagger,
            Map<String, Response> responseMap) throws
            JsonLdError {
        List<com.tinemislej.service.components.parse.api.model.Response> output = new ArrayList<>(responseMap.size());
        for (String responseCode : responseMap.keySet()) {
            Response response = responseMap.get(responseCode);

            if (response.getSchema() != null) {

                List<ResponseEntity> responses = new ArrayList<>();
                tail(response.getSchema(), swagger, responses);

                output.add(new com.tinemislej.service.components.parse.api.model.Response(responseCode, responses));
            } else { // no schema
                output.add(new com.tinemislej.service.components.parse.api.model.Response(responseCode,
                        Collections.emptyList())); // // TODO: 28/12/2017 no semantics?
            }


        }

        return output;
    }

    private String[] getLdType(Object object) throws JsonLdError {

        List<Object> expanded = JsonLdProcessor.expand(object, new JsonLdOptions());

        if (expanded.size() > 1) {
            throw new IllegalStateException("x-semantics should have only 1 object in expanded form");
        } else if (expanded.size() == 0) {
            return null; // TODO return empty instead of null?
        }

        //noinspection unchecked
        Map<String, Object> ld = (Map<String, Object>) expanded.get(0);
        //noinspection unchecked
        List<String> types = (List<String>) ld.get(JsonLdConsts.TYPE);

        return types.toArray(new String[types.size()]);
    }

    private List<ResponseEntity> tail(Property property, Swagger s, List<ResponseEntity> acc) throws JsonLdError {
        if (!(property instanceof RefProperty) && !(property instanceof ArrayProperty) && property.getVendorExtensions().keySet().contains(
                XSEMANTICS)) {
            acc.add(getPropertyAsResponse(property));
        }

        if (property instanceof ArrayProperty) {
            tail(((ArrayProperty) property).getItems(), s, acc);
        } else if (property instanceof RefProperty) {
            RefProperty refProperty = (RefProperty) property;

            Model m = s.getDefinitions().get(refProperty.getSimpleRef());


            acc.add(getModelAsResponse(m));
            Map<String, Property> propertyMap = m.getProperties();
            Set<String> keyset = propertyMap.keySet();
            for (String key : keyset) {
                tail(propertyMap.get(key), s, acc);
            }
        } else if (property instanceof ObjectProperty) {
            ObjectProperty objectProperty = (ObjectProperty) property;

            for (String key : objectProperty.getProperties().keySet()) {
                tail(objectProperty.getProperties().get(key), s, acc);
            }
        }

        return acc;
    }


    private List<Parameter> parseInputParameter(io.swagger.models.parameters.Parameter param, Swagger swagger) throws JsonLdError {

        if (param instanceof BodyParameter) {
            BodyParameter bodyParameter = (BodyParameter) param;
            return parseInputBodyParamter(bodyParameter.getSchema(), swagger,
                    bodyParameter.getRequired());
        }

        Object xSemantics = param.getVendorExtensions().get("x-semantics");

        String[] types = getLdType(xSemantics);


        return Collections.singletonList(new Parameter(types, param.getRequired()));
    }

    private List<Parameter> parseInputBodyParamter(Model model, Swagger swagger,
            boolean required) throws JsonLdError {
        if (model.getVendorExtensions().keySet().contains(XSEMANTICS)) {
            String[] types = getLdType(model.getVendorExtensions().get(XSEMANTICS));

            return Collections.singletonList(new Parameter(types, required));


        } else {
            Map<String, Property> propertyMap = model.getProperties();
            List<Parameter> params = new ArrayList<>(propertyMap.size());

            for (String key : propertyMap.keySet()) {
                params.add(getPropertyAsParameter(propertyMap.get(key), swagger));
            }

            return params;
        }
    }

    private ResponseEntity getPropertyAsResponse(Property p) throws JsonLdError {
        return new ResponseEntity(p.getName(), getLdType(p.getVendorExtensions().get("x-semantics")));
    }

    private Parameter getPropertyAsParameter(Property p, Swagger swagger) throws JsonLdError {
        return new Parameter(getLdType(p.getVendorExtensions().get("x-semantics")), p.getRequired());
    }

    private ResponseEntity getModelAsResponse(Model m) throws JsonLdError {
        return new ResponseEntity(m.getTitle(), getLdType(m.getVendorExtensions().get("x-semantics")));
    }

    private Optional<NonFunctionalParameters> parseNonFunctionalParameters(
            Map<String, Object> vendorExtensions) throws JsonLdError {
        Set<String> keys = vendorExtensions.keySet();

        if (keys.contains("x-nonfunctional-parameters")) {

            NonFunctionalParameters params = new NonFunctionalParameters();

            Object nonFunctionalParameters = vendorExtensions.get("x-nonfunctional-parameters");
            Map<String, Object> compacted = JsonLdProcessor.compact(nonFunctionalParameters, null, new JsonLdOptions());

            for (String key : compacted.keySet()) {
                if (key.equals(JsonLdConsts.TYPE) || key.equals(JsonLdConsts.CONTEXT)) {
                    continue;
                }

                Object value = compacted.get(key);
                if (value instanceof String) { // todo only string values for now
                    StringNonFunctionalParameter param = new StringNonFunctionalParameter(key, (String) value);

                    params.addParameter(param);
                } else if (value instanceof Map) {
                    Map<String, Object> map = (Map) value;

                    String type = (String) map.get(JsonLdConsts.TYPE);
                    if (type.equals(NonFunctionalParameterConstants.DAILY_LIMIT) ||
                            type.equals(NonFunctionalParameterConstants.MONTHLY_LIMIT)) {
                        int limit = (Integer) map.get(NonFunctionalParameterConstants.LIMIT);


                        params.addParameter(new ResponseLimitNonFunctionalParameter(key,
                                new ResponseLimitParameter(type, limit)));
                    }
                }
            }

            return Optional.of(params);
        }

        return Optional.empty();
    }
}
