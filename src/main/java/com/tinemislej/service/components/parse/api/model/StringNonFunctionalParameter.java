package com.tinemislej.service.components.parse.api.model;

public class StringNonFunctionalParameter extends NonFunctionalParameter<String> {

    public StringNonFunctionalParameter(String uri, String value) {
        super(uri, value);
    }
}
