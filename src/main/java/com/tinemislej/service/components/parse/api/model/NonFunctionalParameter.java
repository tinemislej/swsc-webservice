package com.tinemislej.service.components.parse.api.model;

public abstract class NonFunctionalParameter<T> {

    private final String uri;
    private final T value;

    public NonFunctionalParameter(String uri, T value) {
        this.uri = uri;
        this.value = value;
    }

    public String getUri() {
        return uri;
    }

    public T getValue() {
        return value;
    }
}
