package com.tinemislej.service.components.parse.api.model;

import java.util.List;

public class Response {

    private final String code;

    private final List<ResponseEntity> entities;

    public Response(String code, List<ResponseEntity> entities) {
        this.code = code;
        this.entities = entities;
    }

    public String getCode() {
        return code;
    }

    public List<ResponseEntity> getEntities() {
        return entities;
    }
}
