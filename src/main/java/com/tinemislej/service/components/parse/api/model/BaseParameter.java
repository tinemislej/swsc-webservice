package com.tinemislej.service.components.parse.api.model;

public abstract class BaseParameter {

    private final String[] typeUrl;

    public BaseParameter(String[] typeUrl) {
        this.typeUrl = typeUrl;
    }

    public String[] getTypeUrl() {
        return typeUrl;
    }
}
