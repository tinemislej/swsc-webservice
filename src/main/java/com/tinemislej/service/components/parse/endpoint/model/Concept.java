package com.tinemislej.service.components.parse.endpoint.model;

import java.util.HashSet;
import java.util.Set;

public class Concept {

    private final String mainConcept;
    private final Set<String> concepts;

    public Concept(String mainConcept, Set<String> subconcepts) {
        this.concepts = new HashSet<>(subconcepts);
        this.mainConcept = mainConcept;
    }

    public String getMainConcept() {
        return mainConcept;
    }

    public Set<String> getConcepts() {
        return concepts;
    }

    public boolean hasSubConcept(String subconcept) {
        return this.concepts.contains(subconcept);
    }

    public boolean isSubconceptOf(Concept c) {
        return this.mainConcept.equals(c.mainConcept) || c.concepts.contains(mainConcept);
    }

    public Set<String> allConcepts() {
        Set<String> all = new HashSet<>(concepts);
        all.add(mainConcept);
        return all;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Concept concept = (Concept) o;

        return mainConcept != null ? mainConcept.equals(concept.mainConcept) : concept.mainConcept == null;
    }

    @Override
    public int hashCode() {
        return mainConcept != null ? mainConcept.hashCode() : 0;
    }
}

