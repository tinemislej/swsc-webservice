package com.tinemislej.service.components.parse.api.model;

public class ResponseLimitNonFunctionalParameter extends NonFunctionalParameter<ResponseLimitParameter> {

    public ResponseLimitNonFunctionalParameter(String uri, ResponseLimitParameter value) {
        super(uri, value);
    }
}
