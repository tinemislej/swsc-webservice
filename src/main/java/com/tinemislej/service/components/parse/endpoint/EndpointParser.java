package com.tinemislej.service.components.parse.endpoint;

import com.tinemislej.service.components.match.Matcher;
import com.tinemislej.service.components.parse.api.model.Endpoint;
import com.tinemislej.service.components.parse.api.model.Parameter;
import com.tinemislej.service.components.parse.api.model.Response;
import com.tinemislej.service.components.parse.api.model.ResponseEntity;
import com.tinemislej.service.components.parse.endpoint.model.Concept;
import com.tinemislej.service.components.parse.endpoint.model.UiEndpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class EndpointParser {

    private final Matcher matcher;

    @Autowired
    public EndpointParser(Matcher matcher) {
        this.matcher = matcher;
    }

    public Set<UiEndpoint> parseEndpoints(List<Endpoint> endpoints) {
        Set<UiEndpoint> parsed = new HashSet<>(endpoints.size());
        for (Endpoint e : endpoints) {
            UiEndpoint parsedEndpint = parseEndpoint(e);
            parsed.add(parsedEndpint);
        }

        return parsed;
    }

    private UiEndpoint parseEndpoint(Endpoint e) {
        Set<Concept> inputConcept = new HashSet<>(e.getInputParameters().length);
        Set<Concept> outputConcepts = new HashSet<>();

        for (Parameter p : e.getInputParameters()) {
            String mainConcept = p.getTypeUrl()[0]; // TODO
            Set<String> subconcepts = matcher.getSubconcepts(mainConcept);
            inputConcept.add(new Concept(mainConcept, subconcepts));
        }

        for (Response r : e.getResponses()) {
            if (r.getCode().startsWith("2")) { // TODO
                for (ResponseEntity re : r.getEntities()) {
                    String mainConcept = re.getTypeUrl()[0];
                    Set<String> subconcepts = matcher.getSubconcepts(mainConcept);
                    outputConcepts.add(new Concept(mainConcept, subconcepts));
                }
            }
        }

        return new UiEndpoint(e.getUrl(), inputConcept, outputConcepts, e.getNonFunctionalParameters());
    }
}

