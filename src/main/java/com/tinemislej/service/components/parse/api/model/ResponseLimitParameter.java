package com.tinemislej.service.components.parse.api.model;

public class ResponseLimitParameter {

    private final String type;

    private final int value;

    public ResponseLimitParameter(String type, int value) {
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public int getValue() {
        return value;
    }
}
