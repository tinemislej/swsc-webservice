package com.tinemislej.service.components.parse.api.model;


import com.tinemislej.service.components.parse.api.model.NonFunctionalParameters;

import java.util.List;

import javax.annotation.Nullable;

public class Endpoint {

    private final String url;

    private final HttpMethod method;

    private final Parameter[] inputParameters;

    private final List<Response> responses;

    @Nullable  private final NonFunctionalParameters nonFunctionalParameters;

    private Endpoint(String url, HttpMethod method, Parameter[] inputParameters, List<Response> responses, @Nullable  NonFunctionalParameters nonFunctionalParameters) {
        this.url = url;
        this.inputParameters = inputParameters;
        this.responses = responses;
        this.method = method;
        this.nonFunctionalParameters = nonFunctionalParameters;
    }

    public String getUrl() {
        return url;
    }

    public Parameter[] getInputParameters() {
        return inputParameters;
    }

    public List<Response> getResponses() {
        return responses;
    }

    public HttpMethod getMethod() {
        return method;
    }

    @Nullable public NonFunctionalParameters getNonFunctionalParameters() {
        return nonFunctionalParameters;
    }

    public static class Builder{
        private String url;
        private Parameter[] inputParameters;
        private List<Response> responses;
        private HttpMethod method;
        private NonFunctionalParameters nonFunctionalParameters;

        public Builder withUrl(String url){
            this.url = url;
            return this;
        }


        public Builder withInputParameters(Parameter... parameters){
            this.inputParameters = parameters;
            return this;
        }

        public Builder withResponses(List<Response> responseMap){
            this.responses = responseMap;
            return this;
        }

        public Builder withHttpMethod(HttpMethod method){
            this.method = method;
            return this;
        }

        public Builder withNonFunctionalParameters(NonFunctionalParameters parameters){
            this.nonFunctionalParameters = parameters;
            return this;
        }

        public Endpoint build(){
            return new Endpoint(url, method, inputParameters, responses, nonFunctionalParameters);
        }
    }

    public enum HttpMethod{
        GET, POST, PATCH, PUT, DELETE
    }
}
