package com.tinemislej.service.components.parse.api;

public class NonFunctionalParameterConstants {

    public static final String PRICING = "http://sws.org#Pricing";

    public static final String WEB_SERVICE_SECURITY = "http://sws.org#WebServiceSecurity";

    public static final String REQUEST_LIMIT = "http://sws.org#Request_Limit";

    public static final String MONTHLY_LIMIT = "http://sws.org#MonthlyLimit";

    public static final String DAILY_LIMIT = "http://sws.org#DailyLimit";

    public static final String LIMIT = "http://sws.org#Limit";

    public static final String FREE = "http://sws.org#Free";

    public static final String PAID = "http://sws.org#Paid";

    public static final String HIGH_SECURITY = "http://sws.org#HighSecurity";

    public static final String MEDIUM_SECURITY = "http://sws.org#MediumSecurity";

    public static final String LOW_SECURITY = "http://sws.org#LowSecurity";

}
