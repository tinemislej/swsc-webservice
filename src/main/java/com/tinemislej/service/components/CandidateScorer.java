package com.tinemislej.service.components;

import com.tinemislej.service.components.compose.model.EndpointNode;
import com.tinemislej.service.components.compose.model.Node;
import com.tinemislej.service.components.compose.model.Node.NodeType;
import com.tinemislej.service.components.parse.api.NonFunctionalParameterConstants;
import com.tinemislej.service.components.parse.api.model.NonFunctionalParameter;
import com.tinemislej.service.components.parse.api.model.NonFunctionalParameters;
import com.tinemislej.service.components.parse.api.model.ResponseLimitNonFunctionalParameter;
import com.tinemislej.service.components.parse.api.model.ResponseLimitParameter;
import com.tinemislej.service.components.parse.endpoint.model.UiEndpoint;
import com.tinemislej.service.components.repo.EndpointRepo;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

@Service
public class CandidateScorer {

    private EndpointRepo endpointRepo;

    public CandidateScorer(@Autowired EndpointRepo endpointRepo) {
        this.endpointRepo = endpointRepo;
    }

    @Nullable public Double findDailyMax(List<DirectedAcyclicGraph<Node, DefaultWeightedEdge>> candidates) {
        Set<Node> allNodes = new HashSet<>();

        for (DirectedAcyclicGraph<Node, DefaultWeightedEdge> graph : candidates) {
            allNodes.addAll(graph.vertexSet());
        }

        Set<Node> endpointNodes = allNodes.stream().filter(
                node -> node.getNodeType().equals(NodeType.ENDPOINT)).collect(Collectors.toSet());

        Double max = null;

        for (Node n : endpointNodes) {
            UiEndpoint endpoint = endpointRepo.getEndpoint(n.getName());
            if (endpoint.getNonFunctionalParameters() == null) {
                continue;
            }

            for (NonFunctionalParameter param : endpoint.getNonFunctionalParameters().getParameters()) {
                if (param.getUri().equals(NonFunctionalParameterConstants.REQUEST_LIMIT)) {
                    ResponseLimitNonFunctionalParameter rlnp = (ResponseLimitNonFunctionalParameter) param;
                    double divider = rlnp.getValue().getType().equals(
                            NonFunctionalParameterConstants.MONTHLY_LIMIT) ? 30.0 : 1.0;

                    double value = rlnp.getValue().getValue() / divider;

                    if (max == null || value > max) {
                        max = value;
                    }
                }
            }
        }

        return max;
    }

    public Double score(DirectedAcyclicGraph<Node, DefaultWeightedEdge> graph, Double max) {

        Set<Node> endpointNodes = graph.vertexSet().stream().filter(new Predicate<Node>() {
            @Override public boolean test(Node node) {
                return node.getNodeType() == NodeType.ENDPOINT;
            }
        }).collect(Collectors.toSet());

        double score = 0.0;

        for (Node endpointNode : endpointNodes) {
            NonFunctionalParameters nonFunctionalParameters = endpointRepo.getNonFunctionalParameters(
                    endpointNode.getName());

            if (nonFunctionalParameters != null) {
                for (NonFunctionalParameter parameter : nonFunctionalParameters.getParameters()) {
                    score += score(parameter, max);
                }
            }
        }

        return score;
    }

    private Double score(NonFunctionalParameter parameter, Double max) {
        String uri = parameter.getUri();
        Object value = parameter.getValue();

        double score = 0.0;
        switch (uri) {
            case NonFunctionalParameterConstants.PRICING:
                String pricing = (String) value;
                score += scorePricing(pricing);
                break;
            case NonFunctionalParameterConstants.WEB_SERVICE_SECURITY:
                String security = (String) value;
                score += scoreWebServiceSecurity(security);
                break;
            case NonFunctionalParameterConstants.REQUEST_LIMIT:
                ResponseLimitParameter limitParameter = (ResponseLimitParameter) value;
                score += scoreRequestLimit(limitParameter.getType(), limitParameter.getValue(), max);
                break;
        }

        return score;

    }

    private Double scorePricing(String pricingType) {
        return pricingType.equals(NonFunctionalParameterConstants.FREE) ? 1.0 : 0.0;
    }

    private Double scoreWebServiceSecurity(String webServiceSecurityType) {
        switch (webServiceSecurityType) {
            case NonFunctionalParameterConstants.HIGH_SECURITY:
                return 1.0;
            case NonFunctionalParameterConstants.MEDIUM_SECURITY:
                return 0.5;
            case NonFunctionalParameterConstants.LOW_SECURITY:
                return 0.0;
            default:
                return 0.0;
        }
    }

    private Double scoreRequestLimit(String limitType, int value, Double max) {
        double divider = limitType.equals(
                NonFunctionalParameterConstants.MONTHLY_LIMIT) ? 30.0 : 1.0;

        double limitValue = value / divider;

        return -1.0 - (1.0 - limitValue / max);
    }
}
