package com.tinemislej.service.components.repo;

import com.tinemislej.service.components.parse.api.model.NonFunctionalParameter;
import com.tinemislej.service.components.parse.api.model.NonFunctionalParameters;
import com.tinemislej.service.components.parse.api.model.ResponseLimitNonFunctionalParameter;
import com.tinemislej.service.components.parse.api.model.ResponseLimitParameter;
import com.tinemislej.service.components.parse.api.model.StringNonFunctionalParameter;
import com.tinemislej.service.components.parse.endpoint.model.Concept;
import com.tinemislej.service.components.parse.endpoint.model.UiEndpoint;
import com.tinemislej.service.db.DbConcept;
import com.tinemislej.service.db.DbEndpoint;
import com.tinemislej.service.db.DbNonFunctionalParameter;
import com.tinemislej.service.db.DbNonFunctionalParameters;
import com.tinemislej.service.db.DbResponseLimitNonFunctionalParameter;
import com.tinemislej.service.db.DbStringNonFunctionalParameter;
import com.tinemislej.service.db.DbSubconcept;
import com.tinemislej.service.db.repository.EndpointRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

@Service
public class InMemoryEndpointRepo implements EndpointRepo {

    private final EndpointRepository repository;
    private List<UiEndpoint> endpoints;
    private Map<String, UiEndpoint> endpointMap = new HashMap<>();

    @Autowired
    public InMemoryEndpointRepo(EndpointRepository repository) {
        this.repository = repository;
    }


    @Override public List<UiEndpoint> getEndpoints() {
        fetch(); // TODO (ok) only for PoC.
        return endpoints;
    }

    @Override public UiEndpoint getEndpoint(String url) {
        return endpointMap.getOrDefault(url, null);
    }

    @Override public List<UiEndpoint> getEndpointsThatReturnConcept(Concept concept) {
        List<UiEndpoint> endpoints = new ArrayList<>();
        for (String s : endpointMap.keySet()) {
            UiEndpoint e = endpointMap.get(s);

            for (Concept out : e.getOutputConcepts()) {
                if (concept.getMainConcept().equals(out.getMainConcept()) || out.isSubconceptOf(concept)) {
                    endpoints.add(e);
                }
            }
        }
        return endpoints;
    }

    private List<UiEndpoint> parseEndpoints(List<DbEndpoint> dbEndpoints) {
        List<UiEndpoint> endpoints = new ArrayList<>(dbEndpoints.size());
        for (DbEndpoint dbEndpoint : dbEndpoints) {
            UiEndpoint e = new UiEndpoint(dbEndpoint.getUrl(), parse(dbEndpoint.getInputs()),
                    parse(dbEndpoint.getOutputs()),
                    parseNonFunctionalParameters(dbEndpoint.getNonFunctionalParameters()));
            endpoints.add(e);
        }
        return endpoints;
    }

    private Set<Concept> parse(Set<DbConcept> concepts) {
        Set<Concept> uiConcepts = new HashSet<>();
        for (DbConcept c : concepts) {
            Set<String> subconcepts = new HashSet<>();
            for (DbSubconcept dbs : c.getSubconcepts()) {
                subconcepts.add(dbs.getName());
            }
            uiConcepts.add(new Concept(c.getConcept(), subconcepts));
        }
        return uiConcepts;
    }

    private void fetch(){
        this.endpoints = parseEndpoints(
                repository.findAll());

        for (UiEndpoint e : endpoints) {
            endpointMap.put(e.getUrl(), e);
        }
    }

    @Nullable private NonFunctionalParameters parseNonFunctionalParameters(
            @Nullable DbNonFunctionalParameters dbNonFunctionalParameters) {
        if (dbNonFunctionalParameters != null) {
            List<NonFunctionalParameter> nonFunctionalParameters = new ArrayList<>();
            List<DbNonFunctionalParameter> dbParams = dbNonFunctionalParameters.getParameters();
            for (DbNonFunctionalParameter dbParam : dbParams) {
                if (dbParam instanceof DbStringNonFunctionalParameter) {
                    DbStringNonFunctionalParameter dbStringParam = (DbStringNonFunctionalParameter) dbParam;
                    StringNonFunctionalParameter stringParam = new StringNonFunctionalParameter(dbStringParam.getKey(),
                            dbStringParam.getValue());

                    nonFunctionalParameters.add(stringParam);
                } else if (dbParam instanceof DbResponseLimitNonFunctionalParameter) {
                    DbResponseLimitNonFunctionalParameter dbrlnp = (DbResponseLimitNonFunctionalParameter) dbParam;
                    ResponseLimitNonFunctionalParameter rlnp = new ResponseLimitNonFunctionalParameter(dbrlnp.getKey(),
                            new ResponseLimitParameter(dbrlnp.getType(), dbrlnp.getLimit()));

                    nonFunctionalParameters.add(rlnp);
                }
            }

            return new NonFunctionalParameters(nonFunctionalParameters);
        } else {
            return null;
        }

    }

    @Nullable @Override public NonFunctionalParameters getNonFunctionalParameters(String endpointUrl) {
        return endpointMap.get(endpointUrl).getNonFunctionalParameters();
    }
}
