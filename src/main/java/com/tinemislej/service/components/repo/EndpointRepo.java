package com.tinemislej.service.components.repo;

import com.tinemislej.service.components.parse.api.model.NonFunctionalParameters;
import com.tinemislej.service.components.parse.endpoint.model.Concept;
import com.tinemislej.service.components.parse.endpoint.model.UiEndpoint;

import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Nullable;

@Service
public interface EndpointRepo {

    List<UiEndpoint> getEndpoints();

    UiEndpoint getEndpoint(String url);

    List<UiEndpoint> getEndpointsThatReturnConcept(Concept concept);

    @Nullable NonFunctionalParameters getNonFunctionalParameters(String endpointUrl);

}
