package com.tinemislej.service;

import com.tinemislej.service.components.compose.model.Node;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.io.ComponentNameProvider;
import org.jgrapht.io.DOTExporter;

import java.io.FileWriter;

public class Exporter {

    private static final DOTExporter<Node, DefaultWeightedEdge> exporter = new DOTExporter<>(
            new ComponentNameProvider<Node>() {
                @Override
                public String getName(Node component) {
                    return "node_" + Math.abs(component.hashCode());
                }
            }, new ComponentNameProvider<Node>() {
        @Override
        public String getName(Node component) {
            return component.getName();
        }
    }, new ComponentNameProvider<DefaultWeightedEdge>() {
        @Override
        public String getName(DefaultWeightedEdge component) {
            return "";
        }
    });


    public static void export(Graph<Node, DefaultWeightedEdge> graph, String name) {
        try {
            exporter.exportGraph(graph, new FileWriter("/Users/tine/Desktop/" + name));
        } catch (Exception ex) {
        }
    }
}