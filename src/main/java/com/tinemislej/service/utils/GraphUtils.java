package com.tinemislej.service.utils;

import com.tinemislej.service.components.parse.endpoint.model.Concept;

import java.util.Collection;

public final class GraphUtils {

    private GraphUtils() {

    }

    public static boolean containsSubConcept(Collection<Concept> collection, Concept concept){
        for(Concept c : collection){
            if(c.isSubconceptOf(concept)){
                return true;
            }
        }

        return false;
    }
}
