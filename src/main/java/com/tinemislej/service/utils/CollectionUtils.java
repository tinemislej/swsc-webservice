package com.tinemislej.service.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class CollectionUtils {
    private CollectionUtils() {

    }

    public static <T> boolean containsAny(Set<T> thiz, Set<T> that) {
        for (T o1 : thiz) {
            for (T o2 : that) {
                if (o1.equals(o2)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static <T> Set<T> diff(Set<T> thiz, Set<T> that) {
        Set<T> diff = new HashSet<>();
        for (T thizElement : thiz) {
            if (!that.contains(thizElement)) {
                diff.add(thizElement);
            }
        }

        return diff;
    }

    public static <T> List<List<T>> getCombos(List<List<T>> arrs) {
        int n = arrs.size();

        int[] indices = new int[n];

        for (int i = 0; i < n; i++) {
            indices[i] = 0;
        }

        List<List<T>> combos = new ArrayList<>();
        while (true) {
            int next = n - 1;

            List<T> combination = new ArrayList<>(n);
            for (int i = 0; i < n; i++) {
                combination.add(arrs.get(i).get(indices[i]));
            }
            combos.add(combination);

            while (next >= 0 && indices[next] + 1 >= arrs.get(next).size()) {
                next--;
            }

            if (next < 0) {
                return combos;
            }

            indices[next]++;

            for (int i = next + 1; i < n; i++) {
                indices[i] = 0;
            }
        }
    }
}
