# Semantic WS Composition service

SETUP:

* Few environmental variables have to be set in order for program to function.
    * `application.properties`
        * set `spring.datasource.username` to your database user (or set `DB_USER` env. var.)
        * set `spring.datasource.password` to your database user password (or set `DB_PASSWORD` env. var.)
        * set `spring.datasource.url` to your database url - for example: `jdbc:mysql://localhost:3306/wscomposition?useSSL=false` (or set `DB_URL` env. var.)
    * Set `sparql.host` env. var. to SPARQL endpoint url (or modify `sparqlEndpoint` variables in Matcher.java)
    * Set `ONTOLOGY_URL` variable to local path on your machine where ontology is. 
        * Might as well be a URL, then `ontModel.read(...)` must not use FileReader. 
* You can build  jar with gradle using command `./gradlew clean build`.
* Once built, run `java -jar <path to jar>` (should be `build/libs`).

